1. Git pull
2. cd into the repo downloaded
3. cp .env.example .env
4. nano .env to setup the db connection
5. composer install
6. if using mac for this, you may use valet to link the app.
7. npm install
8. npm run dev

- For all backend APIs route can be located at api.php
- For all frontend route can be located at Router.js
