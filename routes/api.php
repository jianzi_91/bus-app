<?php
use Illuminate\Http\Request;

Route::group([], function () {
    Route::post('auth/login', 'AuthController@login');
    Route::post('auth/signup', 'AuthController@signup');
  
    Route::group([
      'middleware' => 'auth:api'
    ], function() {
        Route::get('auth/logout', 'AuthController@logout');
        Route::get('user', 'AuthController@user');

        Route::get('bus-stop/list', 'BusStopController@list');
        Route::get('bus-stop/nearest', 'BusStopController@getNearestBusStop');
        Route::post('bus/add', 'BusController@add');
    });
});