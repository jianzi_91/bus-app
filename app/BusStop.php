<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BusStop extends Model
{
    protected $guarded = [];
    protected $table   = "bus_stops";

    public function buses()
    {
        return $this->hasMany(Bus::class);
    }
}
