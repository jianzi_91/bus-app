<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class BusResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'         => $this->id,
            'bus_number' => $this->bus_number,
            'arrived_at' => $this->arrived_at,
            'bus_stop'   => [
                'id'        => $this->busStop->id,
                'name'      => $this->busStop->name,
                'address'   => $this->busStop->address,
                'latitude'  => $this->busStop->latitude,
                'longitude' => $this->busStop->longitude,
            ],
        ];
    }
}
