<?php

namespace App\Http\Controllers;

use App\Bus;
use App\Http\Resources\BusResource;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class BusController extends Controller
{
    /**
     * Add bus to a bus stop
     */
    public function add(Request $request)
    {
        $data = $request->only('bus_number', 'bus_stop_id', 'arrived_at');

        $v = Validator::make($data, [
            'bus_number'  => 'required|unique:buses,bus_number',
            'bus_stop_id' => 'required|exists:bus_stops,id',
            'arrived_at'  => 'required|date_format:H:i',
        ]);

        if ($v->fails()) {
            return response()->json([
                'msg' => 'Something wrong with the data passed in!',
                'errors' => $v->errors()
            ]);
        }

        $bus = Bus::create([
            'bus_number'  => $data['bus_number'],
            'bus_stop_id' => $data['bus_stop_id'],
            'arrived_at'  => $data['arrived_at'],
        ]);

        return response()->json([
            'msg'  => 'Bus added to bus stop!',
            'data' => [
                'bus'      => new BusResource($bus),
            ]
        ]);
    }
}