<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bus extends Model
{
    protected $guarded = [];
    protected $table   = "buses";
    
    public function busStop()
    {
        return $this->belongsTo(BusStop::class);
    }
}
