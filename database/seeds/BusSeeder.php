<?php

use App\Bus;
use Illuminate\Database\Seeder;

class BusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $buses = [
            [
                'bus_number'  => 'SK123',
                'bus_stop_id' => '1',
                'arrived_at'   => '08:00:00',
            ],
            [
                'bus_number'  => 'SK124',
                'bus_stop_id' => '1',
                'arrived_at'   => '08:15:00',
            ],
            [
                'bus_number'  => 'SK125',
                'bus_stop_id' => '1',
                'arrived_at'   => '09:00:00',
            ],
            [
                'bus_number'  => 'SA221',
                'bus_stop_id' => '2',
                'arrived_at'   => '11:00:00',
            ],
            [
                'bus_number'  => 'SA222',
                'bus_stop_id' => '2',
                'arrived_at'   => '12:00:00',
            ],
            [
                'bus_number'  => 'SA223',
                'bus_stop_id' => '2',
                'arrived_at'   => '12:30:00',
            ],
            [
                'bus_number'  => 'SC321',
                'bus_stop_id' => '3',
                'arrived_at'   => '05:00:00',
            ],
            [
                'bus_number'  => 'SC322',
                'bus_stop_id' => '3',
                'arrived_at'   => '06:00:00',
            ],
            [
                'bus_number'  => 'SC323',
                'bus_stop_id' => '3',
                'arrived_at'   => '07:00:00',
            ],
        ];

        foreach($buses as $bus) {
            Bus::create([
                'bus_number'  => $bus['bus_number'],
                'bus_stop_id' => $bus['bus_stop_id'],
                'arrived_at'  => $bus['arrived_at'],
            ]);
        }
    }
}
