<?php

use App\BusStop;
use Illuminate\Database\Seeder;

class BusStopSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $busStops = [
            [
                'name'      => 'Fengshan Bus Stop',
                'address'   => 'BLK, 187b Bedok North Street 4, #01-52, Singapore 462187',
                'latitude'  => '1.3322664',
                'longitude' => '103.9364883',
            ],
            [
                'name'      => '724 Ang Mo Kio Bus Stop',
                'address'   => '17 Ang Mo Kio Ave 9, Singapore 569766',
                'latitude'  => '1.3721404',
                'longitude' => '103.8445973',
            ],
            [
                'name'      => 'Bukit Timah Bus Stop',
                'address'   => '1 Jalan Anak Bukit #04-01 Bukit Timah Plaza, Singapore 588996',
                'latitude'  => '1.3404994',
                'longitude' => '103.7740163',
            ],
        ];

        foreach($busStops as $busStop) {
            BusStop::create([
                'name'      => $busStop['name'],
                'address'   => $busStop['address'],
                'latitude'  => $busStop['latitude'],
                'longitude' => $busStop['longitude'],
            ]);
        }
    }
}
