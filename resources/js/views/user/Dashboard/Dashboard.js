import React, { Component } from 'react'
import Header from '../../../components/Header/Header';
import Footer from '../../../components/Footer/Footer';
import BusList from '../../../components/BusList';
import AddBus from '../../../components/AddBus';
import { Container, Box, Table, TableBody, TableRow, TableHead, TableCell, Typography } from '@material-ui/core';
import { spacing } from '@material-ui/system';

class Home extends Component {
    constructor() {
        super();
        this.state = {
            isLoggedIn: false,
            user: {},
            isLoaded: false,
            busStop: {},
            buses: {},
        }
    }
    // check if user is authenticated and storing authentication data as states if true
    componentWillMount() {
        let state = localStorage["appState"];
        if (state) {
            let AppState = JSON.parse(state);
            this.setState({ isLoggedIn: AppState.isLoggedIn, user: AppState.user });
            // console.log(AppState.user.id)
        }
    }
    
    componentDidMount() {
        fetch("/api/bus-stop/nearest", {
            method: "GET",
            headers: new Headers({
                'Authorization': 'Bearer ' + this.state.user.access_token,
                'Content-Type': 'application/json'
            })
        })
        .then(res => res.json())
        .then(
            (result) => {
                this.setState({
                    isLoaded: true,
                    busStop: result.data.bus_stop,
                    buses: result.data.bus_stop.buses,
                });
            },
            (error) => {
                this.setState({
                    isLoaded: true,
                    error
                });
            }
        )
    }

    render() {
        return (
            <div>
                <Header userData={this.state.user} userIsLoggedIn={this.state.isLoggedIn} />
                <Container aligncontent="center" alignitems="center">
                    <Box m={10}>
                        <Table>
                            <TableBody>
                                <TableRow>
                                    <TableHead scope="row ">User Id</TableHead>
                                    <TableCell>{this.state.user.id}</TableCell>
                                </TableRow>
                                <TableRow>
                                    <TableHead scope="row ">Full Name</TableHead>
                                    <TableCell>{this.state.user.name}</TableCell>
                                </TableRow>
                                <TableRow>
                                    <TableHead scope="row ">Email</TableHead>
                                    <TableCell>{this.state.user.email}</TableCell>
                                </TableRow>
                            </TableBody>
                        </Table>
                        <div>
                            <Typography variant="h3">Nearest Bus Stop: <strong>{this.state.busStop.name}</strong></Typography>
                            <BusList buses={this.state.buses}/>
                            <AddBus accessToken={this.state.user.access_token}/>
                        </div>
                    </Box>
                </Container>
                <Footer />
            </div>
        )
    }
}
export default Home
