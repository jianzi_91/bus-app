import React, {Component} from 'react'
import {AppBar, Toolbar, Button} from '@material-ui/core';
import {Link, withRouter} from 'react-router-dom';
class Header extends Component {

  constructor(props) {
    super(props);
    this.state = {
      user: props.userData,
      isLoggedIn: props.userIsLoggedIn
    };
    this.logOut = this.logOut.bind(this);
  }

  logOut() {
    let appState = {
      isLoggedIn: false,
      user: {}
    };
    localStorage["appState"] = JSON.stringify(appState);
    this.setState(appState);
    this.props.history.push('/login');
  }

  render() {
    const aStyle = {
      cursor: 'pointer'
    };
    
    return (
      <AppBar>
        <Toolbar>
        <ul>
          <Button color="inherit"><Link to="/">Index</Link></Button>
          {this.state.isLoggedIn ? 
           <Button color="inherit" className="has-sub"><Link to="/dashboard">Dashboard</Link></Button> : ""}
          {this.state.isLoggedIn ? 
           <Button color="inherit" onClick={this.logOut}>Logout</Button> : ""}
          {!this.state.isLoggedIn ?
            <li><Link to="/login">Login</Link> | <Link to="/register">Register</Link></li> : ""}
        </ul>
        </Toolbar>
      </AppBar>
    )
  }
}
export default withRouter(Header)