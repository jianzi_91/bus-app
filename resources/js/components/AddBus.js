import Axios from 'axios';
import React, {Component} from 'react'
import { Fragment } from 'react-is';

class AddBus extends Component {

    constructor(props) {
        super(props);
        this.state = {
            busStops: {},
            formSubmitting: false,
            busStopId: 1,
            busNumber: "",
            arrivedAt: "00:00",
            
        }

        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleBusStopId = this.handleBusStopId.bind(this);
        this.handleBusNumber = this.handleBusNumber.bind(this);
        this.handleArrivedAt = this.handleArrivedAt.bind(this);
    }

    componentDidMount() {
        fetch("/api/bus-stop/list", {
            method: "GET",
            headers: new Headers({
                'Authorization': 'Bearer ' + this.props.accessToken,
                'Content-Type': 'application/json'
            })
        })
        .then(res => res.json())
        .then(
            (result) => {
                this.setState({
                    busStops: result.data,
                });
            },
            (error) => {
                this.setState({
                    error
                });
            }
        )
    }

    handleSubmit(e) {
        e.preventDefault();
        this.setState({formSubmitting: true});

        const headers = {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + this.props.accessToken,
        };
        axios.post("/api/bus/add", {
            "bus_stop_id": this.state.busStopId,
            "bus_number": this.state.busNumber,
            "arrived_at": this.state.arrivedAt
        }, {
            headers: headers
        })
        .then((response) => {
            console.log(response);
            if (response.data.errors) {
                const errors = response.data.errors;
            
                if (errors.bus_number) {
                    window.alert(errors.bus_number);
                }

                if (errors.bus_stop_id) {
                    window.alert(errors.bus_stop_id);
                }

                if (errors.arrived_at) {
                    window.alert(errors.arrived_at);
                }
            } else {
                window.alert("Bus added to the bus Stop!");
            }
        })
        .catch((error) => {
            console.log(error);
        });

        this.setState({formSubmitting: false});
    }

    handleBusStopId(e) {
        let value = e.target.value;
        this.setState({busStopId: e.target.value});
    }

    handleBusNumber(e) {
        let value = e.target.value;
        this.setState({busNumber: e.target.value});
    }

    handleArrivedAt(e) {
        let value = e.target.value;
        this.setState({arrivedAt: e.target.value});
    }

    render() {
        const busStops = this.state.busStops;
        const busStopList = busStops.length ? busStops.map((busStop) => <option key={busStop.id} value={busStop.id}>{busStop.name}</option>) : <option></option>;
        return (
            <Fragment>
                <form onSubmit={this.handleSubmit}>
                    <div className="form-group">
                        <select name="bus_stop_id" onChange={this.handleBusStopId}>{busStopList}</select>
                    </div>
                    <div className="form-group">
                        <input type="text" name="bus_number" placeholder="Bus Number" required onChange={this.handleBusNumber}/>
                    </div>
                    <div className="form-group">
                        <input type="time" name="arrived_at" placeholder="Time of Arrival" required onChange={this.handleArrivedAt}/>
                    </div>
                    <button disabled={this.state.formSubmitting} type="submit" className="btn btn-default btn-lg  btn-block mb10">Add Bus</button>
                </form>
            </Fragment>
        );
    }
}
export default AddBus