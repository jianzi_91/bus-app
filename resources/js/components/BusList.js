import React, {Component} from 'react'
import { Fragment } from 'react-is';
import {List, ListItem} from '@material-ui/core';

class BusList extends Component {

  constructor(props) {
    super(props);
  }

  render() {
    const buses = this.props.buses;
    const busList = buses.length ? buses.map((bus) => <ListItem key={bus.id}>{bus.bus_number + " (" + bus.arrived_at + ")"}</ListItem>) : <ListItem></ListItem>;


    return (
      <Fragment>
        <List>{busList}</List>
      </Fragment>
    );
  }
}
export default BusList